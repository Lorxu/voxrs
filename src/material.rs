use enum_iterator::IntoEnumIterator;

#[derive(IntoEnumIterator, PartialEq, Clone, Copy)]
pub enum Material {
    Air = 0,
    Stone,
    Grass,
    Sand,
    Water = 4,
}
#[derive(Copy, Clone)]
pub struct MatData {
    pub color: [f32; 3],
    pub roughness: f32,
    pub trans: f32,
    pub metal: f32,
    pub ior: f32,
    pub nothing: f32, // Just buffer to pack it in right
}
#[derive(Clone, Copy)]
#[repr(C)]
pub struct AABB {
    min: [f32; 3],
    mat: u32,
    max: [f32; 3],
    children: u32,
}

implement_uniform_block!(AABB, min, max, mat, children);

implement_uniform_block!(MatData, color, roughness, trans, metal, ior, nothing);

/// Returns a vec with all the models in the world, at certain indices.
/// Currently hardcoded, but may read files in the future
pub fn models() -> Vec<AABB> {
    // Note that even though it looks like nodes whose children have children are supported, they won't actually work yet. TODO
    vec![
        // These three nodes are placeholders, serving as an example. Two vertical 'slabs' at either side.
        AABB {
            min: [0.0, 0.0, 0.0],
            max: [1.0, 1.0, 1.0],
            mat: 2,
            children: 2,
        },
        AABB {
            min: [0.0, 0.0, 0.0],
            max: [0.33, 1.0, 1.0],
            mat: 2,
            children: 0,
        },
        AABB {
            min: [0.67, 0.0, 0.0],
            max: [1.0, 1.0, 1.0],
            mat: 2,
            children: 0,
        },
    ]
}

/// Utility function to get a Block for a model index.
/// The u16 in the 3D texture has the highest bit set, indicating a model, and the rest are the index into the `models()` Vec
pub fn model(idx: usize) -> u16 {
    (1 << 15) | idx as u16
}

impl Material {
    pub fn mat_data(&self) -> MatData {
        match self {
            Material::Stone => MatData {
                color: [0.4; 3],
                roughness: 0.2,
                trans: 0.0,
                metal: 0.0,
                ior: 1.45,
                nothing: 0.0,
            },
            Material::Grass => MatData {
                color: [0.3, 0.7, 0.5],
                roughness: 0.8,
                trans: 0.0,
                metal: 0.0,
                ior: 1.45,
                nothing: 0.0,
            },
            Material::Sand => MatData {
                color: [0.9, 0.7, 0.6],
                roughness: 0.6,
                trans: 0.0,
                metal: 0.0,
                ior: 1.45,
                nothing: 0.0,
            },
            Material::Water => MatData {
                color: [0.2, 0.4, 0.9],
                roughness: 0.05,
                trans: 1.0,
                metal: 0.0,
                ior: 1.33,
                nothing: 0.0,
            },
            Material::Air => MatData {
                color: [0.0; 3],
                roughness: 1.0,
                trans: 1.0,
                metal: 0.0,
                ior: 1.0,
                nothing: 0.0,
            },
        }
    }
}
