use super::chunk::Chunks;
use super::common::*;
use super::glm::*;
use super::material::*;
use noise::*;
use rayon::prelude::*;

pub struct Gen {
    noise: HybridMulti,
}

struct Climate {
    height: f32, // -1.0..1.0
    temp: f32, // Celsius
    rain: f32, // 0.0-1.0
}

impl Gen {
    pub fn new() -> Self {
        Gen {
            noise: HybridMulti::new(),
        }
    }

    pub fn gen_chunks(&self) -> Chunks {
        let mut c = Chunks::new();
        let mut i = 0;
        for (z, page) in c.map.iter_mut().enumerate() {
            for (y, row) in page.iter_mut().enumerate() {
                for (x, n) in row.iter_mut().enumerate() {
                    c.chunks[i] =
                        self.gen_chunk(ivec3(x as i32, y as i32, z as i32) - CHUNK_NUM as i32 / 2);
                    *n = i;
                    i += 1;
                }
            }
        }
        c
    }

    pub fn gen_chunk(&self, loc: Vector3<i32>) -> Chunk {
        let mut c = [[[0; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE];
        // let mut full = false;
        let rad = (CHUNK_SIZE as f32) / 2.0;
        //if loc == ivec3(0,0,0) { println!("Why is zero?"); }
        c.par_iter_mut().enumerate().for_each(|(z, row_x)| {
            for (y, row_y) in row_x.iter_mut().enumerate() {
                for (x, b) in row_y.iter_mut().enumerate() {
                    let q = self.gen_block(
                        to_vec3(loc) * (CHUNK_SIZE as f32)
                            + 0.5
                            + vec3((z as f32) - rad, (y as f32) - rad, (x as f32) - rad),
                    );
                    // full = full || q != Material::Air;
                    *b = q as Block;
                }
            }
        });
        if c.iter().flatten().flatten().any(|x| *x != 0) {
            Some(c)
        } else {
            None
        }
    }

    fn hash2(p: Vec2) -> Vec2 {
        let p3 = fract(vec3(p.x,p.y,p.x) * vec3(0.1031,0.1030,0.0973));
        let p3 = p3 + dot(p3, vec3(p3.y,p3.z,p3.x+19.19));
        fract(vec2(p3.x+p3.y,p3.x+p3.z)*vec2(p3.z,p3.y))
    }

    fn voronoi( x: Vec2 ) -> Vec4
    {
        let n = floor(x);
        let f = fract(x);

        //----------------------------------
        // first pass: regular voronoi
        //----------------------------------
    	let (mut mg, mut mr) = (vec2(0.0,0.0),vec2(0.0,0.0));

        let mut md = 8.0;
        let mut e = 0.0;
        for j in -1..2 {
        for i in -1..2
        {
            let g = vec2(float(i),float(j));
    		let o = Self::hash2( n + g );
            let r = g + o - f;
            let d = dot(r,r);

            // Something similar to a gaussian with a random width, as in the article
            let d1 = Self::hash2(o).x+0.15;
    		let e1 = smoothstep(d1,d1+0.3,1.0-length(r));


            e = glm::max(e,e1);
            if d<md
            {
                md = d;
                mr = r;
                mg = g;
            }
        }
        }

        //----------------------------------
        // second pass: distance to borders
        //----------------------------------
        md = 8.0;
        for j in -1..2 {
        for i in -1..2
        {
            let g = mg + vec2(float(i),float(j));
    		let o = Self::hash2( n + g );
            let r = g + o - f;

            if dot(mr-r,mr-r)>0.00001
            { md = glm::min( md, dot( (mr+r)*0.5, normalize(r-mr) ) ) };
        }
        }

        vec4( md, mr.x, mr.y, e )
    }

    fn map(&self, loc: Vector2<f32>) -> Climate {
        let q = Self::voronoi(loc);
        let h = 0.7 * smoothstep(0.3,1.0,self.noise.get(*(to_dvec2(loc)*0.3).as_array()) as f32) * smoothstep(0.8,1.0,1.0-q.x)
            + q.w
            + 0.5 * self.noise.get(*(to_dvec2(loc)*0.05 + 123.34).as_array()) as f32;
        let t =
            q.x * 20.0
            - h * 10.0
            + self.noise.get(*(to_dvec2(loc)*0.01 + 243.19).as_array()) as f32 * 60.0 + 30.0;
        let r =
            (1.0 - h) * (0.2 + 0.5 * (1.0-q.x))
            + 0.4 + 0.2*self.noise.get(*(to_dvec2(loc)*0.05+532.94).as_array()) as f32;
        Climate {
            height: (h*0.7-0.5)*1.2, // Remap to get within the listed values
            temp: t,
            rain: (r+0.5)*0.4,
        }
    }

    fn gen_block(&self, loc: Vector3<f32>) -> Material {
    //    let c = self.map(vec2(loc.x,loc.z)*0.01);
        let h = /*c.height * 12.0;*/self.height(vec2(loc.x, loc.z));
        let surface = if h < 2.0 {
            Material::Sand
        } else {
            Material::Grass
        };

        if abs(loc.y - h) < 1.0 {
            surface
        } else if loc.y < h {
            Material::Stone
        } else if loc.y < 1.0 {
            Material::Water
        } else {
            Material::Air
        }
    }

    fn height(&self, loc: Vector2<f32>) -> f32 {
        12.0 * self.noise.get(*(to_dvec2(loc) * 0.005).as_array()) as f32
    }
}
